package com.stux.booktracker.datasource

import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.Room

@Database(entities = [BookEntity::class], version = 1)
abstract class BookDatabase: RoomDatabase() {
    abstract fun bookDao(): BookDao

    companion object {
        // Marking volatile makes sure that this variable is accessed by only 1 thread at a time
        @Volatile
        private var DATABASE: BookDatabase? = null

        fun getInstance(context: Context): BookDatabase {
            return DATABASE ?: synchronized(this) {
                val instance = Room.databaseBuilder(context.applicationContext, BookDatabase::class.java, "book_db")
                    .enableMultiInstanceInvalidation()
                    .build()
                DATABASE = instance
                instance
            }
        }
    }
}