package com.stux.booktracker.datamodel

import com.stux.booktracker.datasource.BookDao
import com.stux.booktracker.datasource.BookEntity


class BookRepository(private val bookDao: BookDao) {
    suspend fun insert(bookEntity: BookEntity) = bookDao.insert(bookEntity)
    suspend fun update(bookEntity: BookEntity) = bookDao.update(bookEntity)
    suspend fun delete(bookEntity: BookEntity) = bookDao.delete(bookEntity)

    fun unreadBooks() = bookDao.getUnreadBooks()
    fun readBooks() = bookDao.getReadBooks()
}