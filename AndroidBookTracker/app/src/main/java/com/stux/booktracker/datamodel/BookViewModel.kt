package com.stux.booktracker.datamodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.stux.booktracker.datasource.BookDatabase
import com.stux.booktracker.datasource.BookEntity
import kotlinx.coroutines.launch


// The difference b/w ViewModel and AndroidViewModel is that AndroidViewModel holds a reference to
// the application to provide access to Context objects and also has a helper method getApplication()
class BookViewModel(application: Application): AndroidViewModel(application) {
    private val repository: BookRepository
    fun insert(bookEntity: BookEntity) = viewModelScope.launch { repository.insert(bookEntity) }
    fun update(bookEntity: BookEntity) = viewModelScope.launch { repository.update(bookEntity) }
    fun delete(bookEntity: BookEntity) = viewModelScope.launch { repository.delete(bookEntity) }

    fun createAndInsert(isbn: String) = viewModelScope.launch {
        val picture = "https://covers.openlibrary.org/b/isbn/$isbn-M.jpg"
        val title = "From Fatwa to Jihad: How the World Changed: The Satanic Verses to Charlie Hebdo"
        val bookEntity = BookEntity(
            picture = picture,
            name = title,
            isbn = isbn,
            is_complete = false,
            id = null
        )
        this@BookViewModel.insert(bookEntity)
    }

    init {
        val database = BookDatabase.getInstance(application)
        repository = BookRepository(database.bookDao())
    }

    val getUnreadBooks: LiveData<List<BookEntity>> = repository.unreadBooks()
    val getCompletedBooks: LiveData<List<BookEntity>> = repository.readBooks()
}