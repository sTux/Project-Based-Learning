package com.stux.booktracker.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.stux.booktracker.R
import com.stux.booktracker.datasource.BookEntity



class BookRecyclerAdapter(private val books: LiveData<List<BookEntity>>): RecyclerView.Adapter<BookRecyclerAdapter.BookViewHolder>() {

    inner class BookViewHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnLongClickListener {
        val imageView: ImageView? = itemView.findViewById(R.id.bookImage)
        val textView: TextView? = itemView.findViewById(R.id.bookTitle)

        init { itemView.setOnLongClickListener(this) }

        override fun onLongClick(view: View?): Boolean {
            view?.let {
                Snackbar.make(it, "Will show context menu", Snackbar.LENGTH_LONG).show()
            }
            return true
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.layout_book_item, parent, false)

        return BookViewHolder(layout)
    }

    override fun getItemCount(): Int = books.value?.size?: 0

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        with (holder) {
            textView?.text = this@BookRecyclerAdapter.books.value?.get(position)?.name
            imageView?.let {
                Glide.with(it.context)
                     .load(this@BookRecyclerAdapter.books.value?.get(position)?.picture)
                     .thumbnail()
                     .into(it)
            }

        }
    }
}