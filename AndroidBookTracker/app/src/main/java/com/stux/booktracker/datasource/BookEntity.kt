package com.stux.booktracker.datasource

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey


@Entity(tableName = "books", indices = [Index("isbn", unique = true)])
data class BookEntity(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "rowId") val id: Int?,
    @ColumnInfo(name = "isbn") val isbn: String,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "picture") val picture: String?,
    @ColumnInfo(name = "completed", defaultValue = "0") val is_complete: Boolean?
)