package com.stux.booktracker.screens

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import com.stux.booktracker.R
import com.stux.booktracker.datamodel.BookViewModel
import java.lang.IllegalStateException

class AddBookDialog: DialogFragment() {
    private val model: BookViewModel by activityViewModels()
    lateinit var textBox: EditText

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val layout = layoutInflater.inflate(R.layout.layout_bookadd_dialog, null)
        this.textBox = layout.findViewById(R.id.isbn_input)

        return activity?.let { it ->
            val builder = AlertDialog.Builder(it)
            builder.setView(layout)

            builder.setNegativeButton(android.R.string.cancel) { dialog: DialogInterface, _: Int ->
                dialog.dismiss()
            }

            builder.setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                if (this.textBox.text?.isBlank() == false) {
                    this.textBox.text?.toString()?.let { isbn -> model.createAndInsert(isbn) }
                }
            }

            builder.create()
        } ?: throw IllegalStateException("Activity is null")
    }
}