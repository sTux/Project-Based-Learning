package com.stux.booktracker.screens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.stux.booktracker.R
import com.stux.booktracker.adapter.BookRecyclerAdapter
import com.stux.booktracker.datamodel.BookViewModel
import kotlinx.android.synthetic.main.fragment_book_screen.*


class BookListScreen : Fragment() {
    private val viewModel: BookViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(
        R.layout.fragment_book_screen, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bookList.apply {
            adapter = BookRecyclerAdapter(viewModel.getUnreadBooks)
            layoutManager = LinearLayoutManager(this@BookListScreen.context)
        }

        viewModel.getUnreadBooks.observe(viewLifecycleOwner, Observer {
            bookList.adapter?.notifyDataSetChanged()
        })
    }
}