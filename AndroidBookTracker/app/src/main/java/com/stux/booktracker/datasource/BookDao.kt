package com.stux.booktracker.datasource

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.OnConflictStrategy
import androidx.room.Insert
import androidx.room.Delete
import androidx.room.Update
import androidx.room.Query

@Dao
interface BookDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(bookEntity: BookEntity)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun update(bookEntity: BookEntity)

    @Delete
    suspend fun delete(bookEntity: BookEntity)

    @Query("SELECT * FROM books WHERE completed = 0")
    fun getUnreadBooks(): LiveData<List<BookEntity>>

    @Query("SELECT * FROM books WHERE completed = 1")
    fun getReadBooks(): LiveData<List<BookEntity>>
}