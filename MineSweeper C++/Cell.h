#ifndef CELL_H_
#define CELL_H_
#include <ostream>
using namespace std;


class Cell {
    /**
     * This class defines a cell in the minesweeper game.
     * A cell can be of 2 types: normal or bomb.
     * A bomb has cell has the bomb flag set and count always to 9
     * A normal cell has count set to number of bombs adjacent to it.
     * Also, when the discovered flag set in any cell, it's count is display instead of "-"
    **/
    private:
    int count;
    bool bomb;
    bool discovered;

    public:
    Cell(): count(0), bomb(false), discovered(false) {};
    void set_bomb();
    bool is_bomb() {
        return this->bomb;
    }
    void update();
    int get_count() {
        return this->count;
    }
    void discover() {
        // Method to mark a cell as discoverd
        this->discovered = true;
    }
    bool is_seen() {
        // Query if the cell has been discoverd or not
        return this->discovered;
    }
    friend ostream& operator<<(ostream&, Cell&);
};

#endif
