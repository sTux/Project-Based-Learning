#include <experimental/random>
#include <algorithm>
#include <set>
#include <utility>
#include "Board.h"
using namespace std::experimental;
using namespace std;


class Queue {
    /**
     * A FIFO queue that uses a std::set as it's underlying container
     * This was done to ensure only unique entries are inside the queue
    **/
    private:
    set<cell_coord> cell_queue;

    public:
    void enqueue(cell_coord cell) {
        /**
         * Provides the API for queueing a element to the back of the queue
        **/
        this->cell_queue.insert(cell);
    }

    cell_coord dequeue() {
        /**
         * Provides the API for de-queueing a element from the front of the queue
        **/
        set<cell_coord>::iterator begin = this->cell_queue.begin();
        cell_coord cell = *begin;

        this->cell_queue.erase(begin);

        return cell;
    }

    bool is_empty() {
        /**
         * Utility API to help check for a empty queue
        **/
        return this->cell_queue.empty();
    }
};


Board::Board(int rows, int cols) {
    this->n_rows = rows;
    this->n_cols = cols;

    board.assign(rows, vector<Cell>(cols));
    this->populate_mines();
}


void Board::populate_mines() {
    // Randomly select number of mines
    int n_mines = randint(1, static_cast<int>(this->n_cols * this->n_rows * 0.1));
    cerr << "Number of mines: " << n_mines << endl;

    while (n_mines--) {
        // Randomly select a row and column intersection
        int rand_col = randint(1, this->n_cols) - 1;
        int rand_row = randint(1, this->n_rows) - 1;

        this->board[rand_row][rand_col].set_bomb();

        this->populate_neighbour(rand_row, rand_col);
    }
}


vector<cell_coord> Board::neighbours(cell_coord cell) {
    vector<cell_coord> result;
    vector<pair<int, int>> deltas = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}, {-1, -1}, {-1, 1}, {1, -1}, {1, 1}};
    result.resize(deltas.size());

    // Use the std::transform to generate all the possible co-ordinates of neighbours
    transform(deltas.begin(), deltas.end(), result.begin(), [cell](auto delta) {
        return make_pair(cell.first + delta.first, cell.second + delta.second);
    });

    // Use the std::remove_if to remove all the co-ordinates that are out-of-bound for this board.
    // This function actually returns a iterator to the position from where all the illegal entries are present
    auto r = remove_if(result.begin(), result.end(), [this](cell_coord cell) {
        auto [r, c] = cell;

        bool legal_row = ((r >= 0) && (r <= (this->n_rows - 1)));
        bool legal_col = ((c >= 0) && (c <= (this->n_cols - 1)));

        return !(legal_row && legal_col);

    });
    
    // Use the iterator returned to actually erase all the uneeded entries from result
    result.erase(r, result.end());

    return result;
}


void Board::populate_neighbour(int r, int c) {
    vector<cell_coord> valid_neighbours = this->neighbours(make_pair(r, c));

    for_each(valid_neighbours.begin(), valid_neighbours.end(), [this](cell_coord cell_pos) {
        auto[r, c] = cell_pos;
        Cell& cell = this->board[r][c];
        cell.update();
    });
}


int Board::rows() {
    return this->n_rows;
}


int Board::columns() {
    return this->n_cols;
}


ostream& operator<<(ostream& stream, Board& obj) {
    stream << "*|";
    for (int i=1; i <= obj.n_cols; i++)
        stream << i << " ";
    stream << endl;
    for (int i=1; i <= obj.n_cols+1; i++)
        stream << "= ";
    stream << endl;

    int i = 1;

    for_each(obj.board.begin(), obj.board.end(), [&](vector<Cell> r) {
        stream << i++ << "|";
        for_each(r.begin(), r.end(), [&](Cell e) {
            stream << e << " ";
        });
        stream << endl;
    });
    return stream;
}


int Board::at(int row, int col) {
    /**
     * Set the user specified cell as discovered and start disovering other safe cells
     * Returns the number of cell discovered.
     * -1 if the arguments are out of bounds
     * -2 if the arguments points to a bomb
    **/
    int cell_count = 0;
    if ((row > this->n_rows - 1) || (col > this->n_cols - 1) || (row < 0) || (col < 0))
        return -1;

    Queue q;
    q.enqueue(make_pair(row, col));
    Cell& cell = this->board[row][col];

    if (cell.is_bomb()) {
        // User Clicked on a bomb. Terminate the game
        cell.discover();
        return -2;
    } else if (cell.is_seen()) {
        // User clicked on an already discovered cell. Do noting.
        return 0;
    }

    while (!q.is_empty()) {
        // Unpack the cell co-ordinates and mark it as discovered
        auto [row, col] = q.dequeue();
        this->board[row][col].discover();
        cell = this->board[row][col];
        cell_count++;

        if (cell.get_count() > 0)
            // No need to discover neighbours of cells whose counts are > 0, i.e non-zero
            continue;

        for (auto n: this->neighbours(make_pair(row, col))) {
            cell = this->board[n.first][n.second];
            if (!cell.is_seen() && !cell.is_bomb()) {
                q.enqueue(n);
            }
        }
    }

    return cell_count;
}
