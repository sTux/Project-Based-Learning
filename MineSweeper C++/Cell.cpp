#include "Cell.h"


void Cell::set_bomb() {
    /**
     * This method is responsible for properly setting the Cell type to a bomb
    **/
    this->bomb = true;
    this->count = 9;
}


void Cell::update() {
    /**
     * This method is responsible for incrementing the nearby bomb count of a cell
    **/
    if (!this->bomb)
        this->count++;
}


ostream& operator<<(ostream& stream, Cell& cell) {
    /**
     * Method responsible for display the current to the terminal
    **/
    if (cell.discovered) {
        stream << cell.count;
    } else {
        stream << "-";
    }

    return stream;
}
