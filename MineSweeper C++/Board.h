#ifndef MATRIX_CLASS_H
#define MATRIX_CLASS_H
#include <vector>
#include <iostream>
#include "Cell.h"
using namespace std;
using cell_coord = pair<int, int>;
using matrix = vector<vector<Cell>>;


class Board {
    private:
    int n_rows;
    int n_cols;
    matrix board;
    void populate_mines();
    void populate_neighbour(int r, int c);
    vector<cell_coord> neighbours(cell_coord cell);

    public:
    Board(int rows, int cols);
    int at(int row, int col);
    int rows();
    int columns();

    friend ostream& operator<<(ostream& stream, Board& obj);
};

#endif
