# MineSweeper Game written using C++
This project started out as a solution to the Google Tech Dev Challenge's Foundation Path challenge: 

But after completing it, I decided it to use it as a base for implementing a GTK GUI on it, helping to learn to build GUIs using GTK C++ bindings (gtkmm)

The original sources can be found @ [Everything/Google Tech Dev Guide/Foundations Path/Former Coding Interview Question: Minesweeper](https://github.com/RitamDey/Everything/tree/master/Google%20Tech%20Dev%20Guide/Foundations%20Path/Former%20Coding%20Interview%20Question:%20Minesweeper). Last commit id is: [c10761e7756302fc09336622041849147e6a4a5b](https://github.com/RitamDey/Everything/commit/c10761e7756302fc09336622041849147e6a4a5b)


## LICENSE
    BSD 2-Clause License

    Copyright (c) 2019, Ritam Dey
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

