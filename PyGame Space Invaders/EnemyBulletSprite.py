from pygame.sprite import Sprite
from pygame import image
from pygame import transform


class EnemyBulletSprite(Sprite):
    _changeY = 5

    def __init__(self, x, y):
        Sprite.__init__(self)
        self.image = image.load("enemy_bullet.png")
        self.image = transform.rotate(self.image, 180)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def update(self):
        moved_to = self.rect.move(0, self._changeY)
        if moved_to.y > 600:
            self.kill()
        
        self.rect = moved_to
