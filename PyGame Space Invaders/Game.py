import pygame
from pygame import display, time, image, font, mixer, event
pygame.init()
font.init()
mixer.init()
from pygame.sprite import RenderPlain, GroupSingle, spritecollide, groupcollide
from sys import exit
from AlienSprite import AlienSprite
from SpaceshipSprite import SpaceshipSprite


class Game:
    _display = display.set_mode((800, 600))
    _clock = time.Clock()
    _background = image.load("background.png").convert()

    _enemy_bullets = RenderPlain()
    _aliens = RenderPlain((AlienSprite(_enemy_bullets), ))
    _player = GroupSingle(sprite=SpaceshipSprite())
    _bullets = RenderPlain()

    _score = 0
    _running = True

    def __init__(self):
        self._text = font.SysFont(None, 50)
        display.set_icon(image.load("ufo.png"))
        display.set_caption("Space Invaders")
        self._music = mixer.Sound("background.wav")
        self._music.play(-1)
        time.set_timer(pygame.USEREVENT, 30_000)

        # Set the sound track for the explosion
        self._explosion = mixer.Sound("explosion.wav")
    
    def handle_events(self):
        for event in pygame.event.get():
            # Check is the user is asking for us to quit the game. If so then invoke the shutdown routine
            if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.unicode == "q"):
                self.shutdown()
            # Check if the player sprite is initialized or not. Protects un-necessary processing of events when the player sprite has been destroyed
            elif self._player.sprite is None:
                continue
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
                self._player.sprite.move(right=False)
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
                self._player.sprite.move(right=True)
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                self._player.sprite.shoot(group=self._bullets)
            elif event.type == pygame.KEYUP:
                self._player.sprite.move(stop=True)
            elif event.type == pygame.USEREVENT:
                self.fill_aliens()

        display.flip()

    def handle_collisions(self):
        kill_list = groupcollide(self._bullets, self._aliens, True, True)
        for collisions in kill_list:
            self._explosion.play()
            self._score += 1
            self.fill_aliens()
        
        enemy_collided = spritecollide(self._player.sprite, self._aliens, True)
        bullet_collided = spritecollide(self._player.sprite, self._enemy_bullets, True)
        if enemy_collided or bullet_collided:
            self._explosion.play()
            self._aliens.empty()
            self._player.empty()
            self._bullets.empty()
            self._enemy_bullets.empty()
            self._running = False

    def fill_aliens(self):
        if len(self._aliens) < 5:
            self._aliens.add((AlienSprite(self._enemy_bullets), AlienSprite(self._enemy_bullets)))
        elif len(self._aliens) == 5:
            self._aliens.add(AlienSprite(self._enemy_bullets))
    
    def run(self):
        while self._running:
            self._clock.tick(60)

            self._aliens.update()
            self._bullets.update()
            self._player.update()
            self._enemy_bullets.update()
            
            self._display.fill((0, 0, 0))
            self._display.blit(self._background, (0, 0))
            score_surface = self._text.render(f"Score: {self._score}", False, (255, 255, 255))
            self._display.blit(score_surface, (0, 0))

            self._aliens.draw(self._display)
            self._player.draw(self._display)
            self._bullets.draw(self._display)
            self._enemy_bullets.draw(self._display)

            self.handle_events()
            self.handle_collisions()
        else:
            while True:
                text = font.SysFont(None, 100)
                game_over = text.render("GAME OVER", True, (255, 255, 255))
                self._display.blit(game_over, (200, 250))
                self.handle_events()
    
    def shutdown(self):
        self._music.stop()
        display.quit()
        font.quit()
        mixer.quit()
        pygame.quit()
        exit(0)
