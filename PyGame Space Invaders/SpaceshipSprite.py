from pygame.sprite import Sprite
from pygame import image
from pygame.mixer import Sound
from BulletSprite import BulletSprite


class SpaceshipSprite(Sprite):
    # Rate at which the player sprite moves around the screen
    _changeX = 0
    _changeY = 0

    def __init__(self):
        Sprite.__init__(self)
        self.image = image.load("player.png")
        self.rect = self.image.get_rect()
        # Place the player at the (378, 470) position at the display
        self.rect.x = 378
        self.rect.y = 528
        # Load the music for the firing sound
        self._fire = Sound("laser.wav")

    def move(self, right=False, stop=False):
        if stop:
            self._changeX = 0
            return
        if right:
            self._changeX = 3
        else:
            self._changeX = -3
    
    def update(self):
        moved_to = self.rect.move(self._changeX, self._changeY)

        if moved_to.x <= 0:
            moved_to.x = 0
        elif moved_to.x >= 738:
            moved_to.x = 738

        self.rect = moved_to

    def shoot(self, group):
        self._fire.play()
        bullet = BulletSprite(self.rect.x+15, self.rect.y-10)
        group.add(bullet)

