from pygame.sprite import Sprite
from pygame import image


class BulletSprite(Sprite):
    _changeY = -3

    def __init__(self, x, y):
        Sprite.__init__(self)
        self.image = image.load("bullet.png")
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def update(self):
        self.rect.move_ip(0, self._changeY)
