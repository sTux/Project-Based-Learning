from pygame.sprite import Sprite
from pygame import image
from pygame.mixer import Sound
from random import randint
from EnemyBulletSprite import EnemyBulletSprite


class AlienSprite(Sprite):
    # Rates at which the alien enemy moves around the screen
    _changeX = 3
    _changeY = 10
    _fire = Sound("laser.wav")

    def __init__(self, bullet_group):
        # Load the enemy alien icon and move it the determined position
        Sprite.__init__(self)
        self.image  = image.load("enemy.png")
        self.rect = self.image.get_rect()
        # Place the alien at the (0, 0) of the display
        self.rect.x = randint(0, 700)
        self.rect.y = randint(0, 236)
        # A reference to the bullet group that will hold the sprites for the enemy bullets
        self.bullet_group = bullet_group
        self.fire()
    
    def update(self):
        # On every update of the sprit, move the spirt right towards the right end of the screen.
        # After the sprit has "collided" with the right end of the screen, then move the sprit down and keep moving to the left and vice-versa
        if len(self.bullet_group) < 1:
            self.fire()
        moved_to = self.rect.move(self._changeX, 0)
        if moved_to.x >= 736 or moved_to.x <= 0:
            moved_to.move_ip(0, self._changeY)
            self._changeX = -self._changeX
        self.rect = moved_to

    def fire(self):
        bullet_x = self.rect.x + 20
        bullet_y = self.rect.y + 30
        bullet = EnemyBulletSprite(bullet_x, bullet_y)
        self.bullet_group.add(bullet)
        self._fire.play()
